﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using TickerTape.Models;
using TickerTape.Utilities;

namespace TickerTape.Models
{
    /// <summary>
    /// Type that indicates how serialized quotes should be returned.
    /// </summary>
    internal enum QuoteObjectType : byte
    {
        AsSerializedQuote = 0,
        AsJsonObject
    }

    /// <summary>
    /// Type that indicates return value of quote DataResponse.
    /// </summary>
    internal enum ResponseCode : byte
    {
        OperationTimeout = 0,
        NoObjectRecieved,
        OK,
        Error
    }

    internal static class QuoteData
    {
        internal static object GetResponse(string args, QuoteObjectType qType = QuoteObjectType.AsJsonObject)
        {
            try
            {
                using (var response = QuoteWebUtility.BuildWebResponse(args))
                {
                    using (var rdr = new StreamReader(response.GetResponseStream()))
                    {
                        switch (qType)
                        {
                            case QuoteObjectType.AsSerializedQuote: return ParseDataResponse(rdr);
                            case QuoteObjectType.AsJsonObject: return JToken.ReadFrom(new JsonTextReader(rdr));
                            default: return null;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                var errResp = ResponseQuery.Empty;
                errResp.Status = (e is WebException) ? ResponseCode.OperationTimeout : ResponseCode.Error;
                return errResp;
            }
        }

        private static ResponseQuery ParseDataResponse(StreamReader stream)
        {
            try
            {
                var resp = JsonConvert.DeserializeObject<ResponseQuery>(stream.ReadToEnd());
                resp.Status = ResponseCode.OK;
                return resp;
            }
            catch (Exception e)
            {
                var errResp = new ResponseQuery();
                if (e is WebException)
                    errResp.Status = ResponseCode.OperationTimeout;
                return errResp;
            }
        }

        internal static JToken GetQuoteTokenObject(string args)
        {
            try
            {
                return ((JObject)GetResponse(args))["query"]["results"]["quote"];
            }
            catch (Exception e) { ErrorUtility.Log(e); return null; }
        }
    }
}
