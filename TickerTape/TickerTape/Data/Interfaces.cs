﻿using System;

namespace TickerTape.Interfaces
{
    interface IModifiableState
    {
        bool IsModified { get; set; }
    }
}
