﻿using Newtonsoft.Json;

using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

using TickerTape.Models;

namespace TickerTape.Utilities
{
    public static class SettingsUtility
    {
        internal const string TempWatchList = "twl.dat";
        internal const string WatchListFile = "watchlist.dat";
        internal const string ConfigFile = "config.dat";

        internal static readonly string ApplicationSavePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\TickerTape";
        private static readonly string SettingsFilePath = ApplicationSavePath + "\\" + ConfigFile;

        public static QuoteSetViewModel LoadWatchList(string path = "")
        {
            if (string.IsNullOrWhiteSpace(path))
            {
                StringBuilder pathBldr = new StringBuilder(ApplicationSavePath);
                pathBldr.Append("\\");

                if (File.Exists(pathBldr.ToString() + WatchListFile))
                    pathBldr.Append(WatchListFile);
                else if (File.Exists(pathBldr.ToString() + TempWatchList))
                    pathBldr.Append(TempWatchList);
                else
                    return QuoteSetViewModel.Empty;

                path = pathBldr.ToString();
            }

            try
            {
                if(JSONUtility.CheckFormat(path))
                    return JSONUtility.LoadJSONObject<QuoteSetViewModel>(path);
            }
            catch (Exception e) { Console.WriteLine(e.StackTrace); }
            return QuoteSetViewModel.Empty;
        }

        internal static Settings BuildDefaultSettingsFile()
        {
            if (!Directory.Exists(ApplicationSavePath))
                Directory.CreateDirectory(ApplicationSavePath);

            Settings newSettings = Settings.Default;
            newSettings.Save();
            return newSettings;
        }

        internal static Settings LoadSettings()
        {
            Settings set = null;
            if(JSONUtility.CheckFormat(SettingsFilePath))
                set = JSONUtility.LoadJSONObject<Settings>(SettingsFilePath);

            if(set != default(Settings)){
                return set;
            }
            return BuildDefaultSettingsFile();
        }

        internal static void SaveSettings(Settings settings){
            try
            {
                if (DirectoryUtility.TryMakePathSafe(SettingsFilePath))
                    JSONUtility.SaveJSONObject<Settings>(SettingsFilePath, settings);
                else
                    throw new FileLoadException();
            }
            catch (Exception e) { ErrorUtility.Log(e, "An error occurred trying to save object '" + typeof(Settings).Name + "'."); }
        }

        internal static bool IsBetween(int target, int a, int b)
        {
            return (target >= a && target <= b);
        }
    }
}
