﻿using System;
using System.Text;

using TickerTape.Models;

namespace TickerTape.Utilities
{
    internal static class AlertUtility
    {
        internal static bool IsWithinRange(object sender, Alert.AlertArgs args)
        {
            return false;
        }

        internal static bool IsPastTarget(object sender, Alert.AlertArgs args)
        {
            return false;
        }

        internal static bool MatchTrailingStop(object sender, Alert.AlertArgs args)
        {
            return false;
        }
    }
}
