﻿using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Text;

namespace TickerTape.Utilities
{
    public static class DirectoryUtility
    {
        internal static bool TryMakePathSafe(string p)
        {
            if (File.Exists(p))
            {
                // Try Open
                try
                {
                    using (Stream str = new FileStream(p, FileMode.Open)) { }
                }
                catch (Exception e)
                {
                    // File in use, return
                    ErrorUtility.Log(e);
                    return false;
                }

                File.Delete(p);
                return true;
            }

            return false;
        }
    }
}