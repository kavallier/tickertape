﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using TickerTape.Models;

namespace TickerTape.Utilities
{
    internal static class QuoteWebUtility
    {
        private const string QueryBase = @"http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(";
        private const string QueryParams = @")&format=json&diagnostics=false&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        private const string HtmlCommanSymbol = "%2C";
        private const string HtmlSpaceCharacter = "%22";
        private const string RegexBaseSymbolPattern = @"^([a-zA-Z]+,?)+$";
        private const string RegexAlphaPattern = @"^[a-zA-Z]$";
        private const string RegexNumericPattern = @"^(D|NumPad)[0-9]$";

        internal static bool IsAlphanumeric(string input)
        {
            return Regex.IsMatch(input, RegexAlphaPattern);
        }

        internal static bool IsNumeric(string input)
        {
            return Regex.IsMatch(input, RegexNumericPattern);
        }

        internal static bool IsCleanSymbolSet(string input)
        {
            return Regex.IsMatch(input, RegexBaseSymbolPattern);
        }

        internal static string GetQueryURL(string args)
        {
            if (IsCleanSymbolSet(args))
            {
                StringBuilder strBldr = new StringBuilder(QueryBase);
                var temp = args.Split(new string[] { ",", ", " }, StringSplitOptions.RemoveEmptyEntries);
                for (int x = 0; x < temp.Length; x++)
                {
                    if (x > 0)
                        strBldr.Append(HtmlCommanSymbol);
                    strBldr.Append(HtmlSpaceCharacter);
                    strBldr.Append(temp[x]);
                    strBldr.Append(HtmlSpaceCharacter);
                }
                strBldr.Append(QueryParams);

                return strBldr.ToString();
            }
            return null;
        }

        internal static WebResponse BuildWebResponse(string args)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(GetQueryURL(args));
                request.Timeout = 1500;
                request.Proxy = null;

                return (WebResponse)request.GetResponse();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                throw e;
            }
        }
    }
}
