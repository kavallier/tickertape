﻿using System;
using System.ComponentModel;
using System.Text;

namespace TickerTape.Utilities.Extensions
{
    public static class Extensions
    {
        public static string GetDescription<T>(this T enumVal)
            where T : struct
        {
            var type = enumVal.GetType();
            if (!type.IsEnum)
                throw new ArgumentException("Value passed must be of Enum Type.");

            var memInfo = type.GetMember(enumVal.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                var description = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (description != null && description.Length > 0)
                    return ((DescriptionAttribute)description[0]).Description;
            }
            return enumVal.ToString();
        }
    }
}
