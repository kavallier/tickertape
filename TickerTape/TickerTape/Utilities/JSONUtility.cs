﻿using System;
using System.IO;

namespace TickerTape.Utilities
{
    public static class JSONUtility
    {
        public static bool CheckFormat(string path)
        {
            try
            {
                using (var fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    using (var rdr = new StreamReader(fs))
                    {
                        var str = rdr.ReadLine().TrimStart();
                        return (str.StartsWith("{") || str.StartsWith("["));
                    }
                }
            }
            catch (Exception e) { ErrorUtility.Log(e); }
            return false;
        }

        public static T LoadJSONObject<T>(string path)
        {
            T tmp = default(T);
            try
            {
                using (var fs = new FileStream(path, FileMode.Open))
                {
                    using (var rdr = new StreamReader(fs))
                    {
                        using (var jsonRdr = new Newtonsoft.Json.JsonTextReader(rdr))
                        {
                            Newtonsoft.Json.JsonSerializer jsr = new Newtonsoft.Json.JsonSerializer();
                            tmp = jsr.Deserialize<T>(jsonRdr);
                            jsonRdr.Close();
                        }
                    }
                }
                return tmp;
            }
            catch (Exception e) { ErrorUtility.Log(e); tmp = default(T); }
            return tmp;
        }

        public static void LoadJSONObject<T>(string path, out T output)
        {
            output = LoadJSONObject<T>(path);
        }

        public static T SaveJSONObject<T>(string path, T obj)
        {
            try
            {
                using (var fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    using (var wrtr = new StreamWriter(fs))
                    {
                        using (var jsonWrtr = new Newtonsoft.Json.JsonTextWriter(wrtr))
                        {
                            jsonWrtr.Formatting = Newtonsoft.Json.Formatting.Indented;
                            Newtonsoft.Json.JsonSerializer jsr = new Newtonsoft.Json.JsonSerializer();
                            jsr.Serialize(jsonWrtr, obj);
                            jsonWrtr.Close();
                        }
                    }
                }
            }
            catch (Exception e) { ErrorUtility.Log(e); }
            return obj;
        }
    }
}
