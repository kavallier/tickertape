﻿using System;
using System.Text;

namespace TickerTape.Utilities
{
    public static class ErrorUtility
    {
        public const string GeneralError = "An error has occurred. Please check log for details.";
        public const string TimeoutError = "Connection timed out. Please try again.";
        private static readonly string LogPath = SettingsUtility.ApplicationSavePath + @"\log.txt";
        private static readonly StringBuilder ErrorBuilder = new StringBuilder(String.Empty);

        public static void Log(Exception e, string appendMessage = "", bool isDebug = false)
        {
            if (!string.IsNullOrWhiteSpace(appendMessage))
                appendMessage = appendMessage + " - ";

            ErrorBuilder.Clear().Append(appendMessage).Append(e.Message);

            if (isDebug)
                ErrorBuilder.AppendLine().Append(e.StackTrace);

            Log(ErrorBuilder.ToString());
        }

        public static void Log(string message)
        {
            ErrorBuilder.Clear().Append(DateTime.Now.ToString());
            ErrorBuilder.Append(" Log Message: ").AppendLine(message);

            LogToFile(ErrorBuilder.ToString());
        }

        private static void LogToFile(string m)
        {
            using (var wrt = System.IO.File.AppendText(LogPath))
            {
                wrt.Write(m);
            }
        }
    }
}
