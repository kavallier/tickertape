﻿using Newtonsoft.Json;

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

using TickerTape.Interfaces;
using TickerTape.Utilities;

namespace TickerTape.Models
{
    [DataContract]
    public class Settings : IModifiableState
    {
        private int _defaultTab = 0;
        private int _updateFrequency = 15;
        private bool _showLastPriceOnQueryError = false;

        private static readonly Settings _defaultSettings = new Settings();

        [IgnoreDataMember]
        public bool IsModified { get; set; }

        [IgnoreDataMember]
        public static Settings Default { get { return _defaultSettings; } }

        /// <summary>
        /// Get save path for application settings.
        /// </summary>
        [IgnoreDataMember]
        public string SavePath
        {
            get { return SettingsUtility.ApplicationSavePath; }
            private set { }
        }

        [DataMember(Name = "updateFrequency")]
        public int UpdateFrequency
        {
            get { return _updateFrequency; }
            set
            {
                _updateFrequency = SetValue(value);
                IsModified = true;
            }
        }

        [DataMember(Name = "showLastPriceOnQueryError")]
        public bool ShowLastPriceOnQueryError
        {
            get { return _showLastPriceOnQueryError; }
            set
            {
                _showLastPriceOnQueryError = value;
                IsModified = true;
            }
        }

        [DataMember(Name = "defaultTab")]
        public int DefaultTab
        {
            get { return _defaultTab; }
            set
            {
                _defaultTab = SetValue(value);
                IsModified = true;
            }
        }

        [DataMember(Name = "debug")]
        public bool Debug { get; set; }

        public Settings()
        {
            IsModified = false;
        }

        public Settings(bool asNew = false)
        {
            IsModified = asNew;
        }

        private int SetValue(int value)
        {
            IsModified = true;
            if (value > 0)
                return value;
            return 0;
        }

        private string SetValue(string value)
        {
            IsModified = true;
            if(!string.IsNullOrEmpty(value))
                return value;
            return string.Empty;
        }

        public TimeSpan GetUpdateTimeSpan()
        {
            return new TimeSpan((int)(_updateFrequency / 3600), (int)(_updateFrequency / 60), _updateFrequency % 60);
        }

        public void Save()
        {
            SettingsUtility.SaveSettings(this);
            IsModified = false;
        }

        public static Settings Load()
        {
            return SettingsUtility.LoadSettings();
        }
    }
}
