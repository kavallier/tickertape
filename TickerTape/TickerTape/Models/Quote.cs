﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

using TickerTape.Models;

namespace TickerTape.Models
{
    [DataContract]
    public struct QuoteSymbol
    {
        [DataMember(Name = "symbol")]
        public string Symbol;

        [DataMember(Name = "alerts")]
        public Alert[] Alerts;

        public QuoteSymbol(string symb, Alert[] alerts)
        {
            Symbol = symb;
            Alerts = alerts;
        }
    }

    [DataContract]
    public class Quote
    {
        public const string NOVALUE = "N/A";

        private List<Alert> _alerts;

        [IgnoreDataMember]
        public bool IsNew { get; set; }

        [DataMember(Name="symbol")]
        public string Symbol{ get; set; }

        [DataMember(Name="AverageDailyVolume")]
        public string AverageDailyVolume { get; set; }

        [DataMember(Name="Change")]
        public string Change { get; set; }

        [DataMember(Name="DaysLow")]
        public string Low { get; set; }

        [DataMember(Name="DaysHigh")]
        public string High { get; set; }

        [DataMember(Name="YearHigh")]
        public string YearHigh { get; set; }

        [DataMember(Name="YearLow")]
        public string YearLow { get; set; }

        [DataMember(Name="MarketCapitalization")]
        public string MarketCap { get; set; }

        [DataMember(Name="LastTradePriceOnly")]
        public string LastTrade { get; set; }

        [DataMember(Name="DaysRange")]
        public string Range { get; set; }

        [DataMember(Name="Name")]
        public string CompanyName { get; set; }

        [DataMember(Name="Volume")]
        public string ShareVolume { get; set; }

        [DataMember(Name = "Ask")]
        public string AskPrice { get; set; }

        [DataMember(Name = "Bid")]
        public string BidPrice { get; set; }

        [DataMember(Name = "Open")]
        public string Open { get; set; }

        [DataMember(Name = "alerts")]
        public List<Alert> Alerts
        {
            get
            {
                if (_alerts == null)
                    _alerts = new List<Alert>();
                return _alerts;
            }
            set
            {
                _alerts = value;
            }
        }

        [IgnoreDataMember()]
        public DateTime Created { get; set; }

        [IgnoreDataMember()]
        public string SymbolWithPrice
        {
            get
            {
                if (Symbol != "N/A" || !string.IsNullOrWhiteSpace(Symbol))
                    return this.Symbol + Environment.NewLine + GetCurrentPriceString();
                return "N/A";
            }
        }

        [JsonConstructor]
        public Quote() { Update(); }

        public Quote(Quote quote) { Update(quote); }

        public string GetCurrentPriceString()
        {
            if (this.LastTrade != NOVALUE)
                return this.LastTrade + " (" + this.Change + ")";
            return "0.00 N/A";
        }

        public string ToCompanyString()
        {
            if (this.CompanyName != NOVALUE)
            {
                return this.CompanyName + " - " + this.Symbol;
            }
            return this.CompanyName;
        }

        public string GetPercentChange()
        {
            if (!string.IsNullOrEmpty(this.Change) && !this.Change.Equals(Quote.NOVALUE))
            {
                float open, change = 0f;
                float.TryParse(this.Open, out open);
                float.TryParse(this.Change.Substring(this.Change.IndexOf("0.00")), out change);
                string label = (this.Change.Contains("+")) ? "+" : "-";
                return (label + (change / open).ToString());
            }
            return Quote.NOVALUE;
        }

        public void Update(Quote quote = null)
        {
            if (quote != null)
            {
                Created = DateTime.Now;
                Symbol = quote.Symbol;
                AverageDailyVolume = quote.AverageDailyVolume;
                Change = quote.Change;
                Low = quote.Low;
                High = quote.High;
                YearHigh = quote.YearHigh;
                YearLow = quote.YearLow;
                MarketCap = quote.MarketCap;
                LastTrade = quote.LastTrade;
                Range = quote.Range;
                CompanyName = quote.CompanyName;
                ShareVolume = quote.ShareVolume;
                AskPrice = quote.AskPrice;
                BidPrice = quote.BidPrice;
                Open = quote.Open;
                IsNew = false;

                if (quote.Alerts.Count > 0)
                {
                    for (int x = 0; x < quote.Alerts.Count; x++)
                        this.Alerts.Add(new Alert(quote.Alerts[x]));
                }
            }
            else
            {
                Symbol = NOVALUE;
                AverageDailyVolume = NOVALUE;
                Change = NOVALUE;
                Low = NOVALUE;
                High = NOVALUE;
                YearHigh = NOVALUE;
                YearLow = NOVALUE;
                MarketCap = NOVALUE;
                LastTrade = NOVALUE;
                Range = NOVALUE;
                CompanyName = NOVALUE;
                ShareVolume = NOVALUE;
                AskPrice = NOVALUE;
                BidPrice = NOVALUE;
                Open = NOVALUE;
                IsNew = true;
            }
        }
    }

    public class QuoteConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType) { return (objectType == typeof(Quote[])); }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JToken jTkn = JToken.Load(reader);
            if (jTkn.Type == JTokenType.Object)
                return new Quote[] { jTkn.ToObject<Quote>() };
            else if (jTkn.Type == JTokenType.Array)
                return jTkn.ToObject<Quote[]>();
            throw new JsonSerializationException("Unexpected token type: " + jTkn.Type.ToString());
        }

        // TODO
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }
    }
}
