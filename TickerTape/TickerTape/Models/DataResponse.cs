﻿using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Text;

using TickerTape.Models;
using TickerTape.Utilities;

namespace TickerTape.Models
{
    [DataContract]
    internal class ResponseQuery
    {
        public static readonly ResponseQuery Empty = new ResponseQuery();

        [DataMember(Name = "query")]
        internal ResponseQueryResults Query { get; set; }

        [IgnoreDataMember()]
        internal ResponseCode Status { get; set; }

        public ResponseQuery()
        {
            Status = ResponseCode.NoObjectRecieved;
        }

        public ResponseQuery(ResponseCode rc)
        {
            Status = rc;
        }

        public Quote GetFirst()
        {
            try
            {
                if(this.Status == ResponseCode.OK)
                    return this.Query.Result.Quotes[0];
            }
            catch (Exception e) { ErrorUtility.Log(e); }
            return new Quote();
        }

        public Quote[] GetAll()
        {
            try
            {
                if (this.Status == ResponseCode.OK)
                    return this.Query.Result.Quotes;
            }
            catch (Exception e) { ErrorUtility.Log(e); }
            return new Quote[] { new Quote() };
        }
    }

    [DataContract]
    internal class ResponseQueryResults
    {
        [DataMember(Name = "results")]
        internal ResponseQueryQuoteSet Result { get; set; }
    }

    [DataContract]
    internal class ResponseQueryQuoteSet
    {
        [DataMember(Name = "quote")]
        [JsonConverter(typeof(QuoteConverter))]
        internal Quote[] Quotes { get; set; }
    }
}
