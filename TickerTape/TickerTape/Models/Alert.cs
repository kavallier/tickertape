﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Text;

using TickerTape.Utilities.Extensions;

namespace TickerTape.Models
{
    [DataContract]
    public class Alert : TickerTape.Interfaces.IModifiableState
    {
        public enum AlertType : byte
        {
            [Description("New Alert")]
            None = 0,
            [Description("Price Range")]
            Range,
            [Description("Greater Than")]
            GreaterThan,
            [Description("Less Than")]
            LessThan,
            [Description("Trailing Stop")]
            TrailingStop
        }

        public class AlertArgs
        {
            public AlertType Type { get; set; }
            public double[] Arguments { get; set; }

            public AlertArgs(AlertType type, double[] args)
            {
                Type = type;
                Arguments = args;
            }
        }

        public delegate void AlertEventHandler(object sender, AlertArgs arg);

        public event AlertEventHandler CalculatingAlert;

        [DataMember(Name = "alertLabel")]
        public string AlertLabel { get; set; }

        [DataMember(Name = "targetValue")]
        public float [] TargetValue { get; set; }

        [DataMember(Name = "alertType")]
        public AlertType Type { get; set; }

        [IgnoreDataMember]
        public bool IsActive { get; set; }

        [IgnoreDataMember]
        public bool IsModified { get; set; }

        [IgnoreDataMember]
        public string AlertName
        {
            get
            {
                string temp = string.Empty;
                if(string.IsNullOrWhiteSpace(AlertLabel))
                    temp = Type.GetDescription();
                else
                    temp = AlertLabel;

                if (IsModified)
                    temp += "*";

                return temp;
            }
            set { AlertLabel = value; }
        }

        [JsonConstructor]
        public Alert(AlertType type, params float[] target)
        {
            TargetValue = target;
            Type = type;
            IsModified = false;
        }

        public Alert(AlertType type, AlertEventHandler ev, params float[] target)
            : this(type, target)
        {
            CalculatingAlert += ev;
        }

        public Alert(AlertType type, AlertEventHandler ev, string label = "", params float[] target)
            : this(type, ev, target)
        {
            AlertLabel = label;
        }

        public Alert(Alert al) : this(al.Type, al.CalculatingAlert, al.TargetValue) { }
    }
}
