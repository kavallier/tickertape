﻿using Microsoft.Win32;

using Newtonsoft.Json;

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

using TickerTape.Models;
using TickerTape.Utilities;

namespace TickerTape
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainView : Window
    {
        BackgroundWorker BackgroundQuotes;
        DispatcherTimer QuoteUpdateTimer;

        Quote OpenQuote;
        QuoteSetViewModel SymbolList;
        Settings ApplicationSettings;

        StringBuilder DataBuilder;

        // TEST
        Random rand = new Random();

        int lastIndex = -1;
        ResponseCode DataResponseStatus = ResponseCode.OK;

        public MainView()
        {
            InitializeComponent();

            ApplicationSettings = Settings.Load();

            if (SettingsUtility.IsBetween(ApplicationSettings.DefaultTab, 0, cbDefaultTab.Items.Count - 1))
                ApplicationSettings.DefaultTab = 0;

            if (!SettingsUtility.IsBetween(ApplicationSettings.UpdateFrequency, (int)sldUpdateFrequency.Minimum, (int)sldUpdateFrequency.Maximum))
                ApplicationSettings.UpdateFrequency = (int)sldUpdateFrequency.Minimum;

            SymbolList = SettingsUtility.LoadWatchList();
            SymbolList.CollectionChanged += WatchListChanged;
            lbWatchList.ItemsSource = SymbolList;

            QuoteUpdateTimer = new DispatcherTimer();
            QuoteUpdateTimer.Tick += UpdateWatchList;

            BackgroundQuotes = new BackgroundWorker();
            BackgroundQuotes.DoWork += GetQuotes;
            BackgroundQuotes.ProgressChanged += UpdateViewUI;
            BackgroundQuotes.RunWorkerCompleted += (s,e) => {
                if(!BackgroundQuotes.CancellationPending)
                    UpdateView(tabController.SelectedIndex);
            };
            BackgroundQuotes.WorkerSupportsCancellation = true;
            BackgroundQuotes.WorkerReportsProgress = true;
            
            OpenQuote = new Quote();
            tabController.SelectedIndex = ApplicationSettings.DefaultTab;

            DataBuilder = new StringBuilder(String.Empty);

            foreach (TabItem tab in tabController.Items)
                cbDefaultTab.Items.Add(((TextBlock)tab.Header).Text);

            UpdateActiveSettings();

            if (SymbolList.Count > 0)
            {
                // Add Alert test save
                //SymbolList[0].Alerts.Add(new Alert(Alert.AlertType.LessThan, null, 20.0f));

                // Existing Force update
                ForceWatchUpdate(this, EventArgs.Empty);
            }
        }

        private void UpdateViewUI(object sender, ProgressChangedEventArgs e)
        {
            var updates = (Quote[])e.UserState;
            for (int x = 0; x < SymbolList.Count; x++)
            {
                SymbolList.Update(x,updates[x]);
            }
            updates = null;

            WLRefreshTime.Text = "Last Updated: " + DateTime.Now.ToLongTimeString();
        }

        private void UpdateView(int tab, bool makeEmpty = false)
        {
            if (tab <= 1 && ApplicationSettings.IsModified)
                ApplicationSettings.IsModified = false;

            menuSaveWatchList.IsEnabled = (SymbolList.Count > 0);

            switch (tab)
            {
                case 0:
                    switch(DataResponseStatus){
                        case ResponseCode.Error: txtBlockStockName.Text = ErrorUtility.GeneralError;
                            break;
                        case ResponseCode.OperationTimeout: txtBlockStockName.Text = ErrorUtility.TimeoutError;
                            break;
                        default: txtBlockStockName.Text = OpenQuote.ToCompanyString();
                            break;
                    }
                    txtBlockStockPrice.Text = OpenQuote.GetCurrentPriceString();
                    txtBlockStockAsk.Text = OpenQuote.AskPrice;
                    txtBlockStockBid.Text = OpenQuote.BidPrice;
                    txtBlockStockVol.Text = OpenQuote.MarketCap;
                    bAddWatchlist.Visibility = (!OpenQuote.IsNew) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;

                    bQuickSearch.IsEnabled = true;
                    break;
                case 1:
                    if (SymbolList.Count == 0)
                    {
                        WLRefreshTime.Text = "Last Updated: N\\A";
                        btnRemoveFromList.Visibility = Visibility.Hidden;
                        txtBlockWatchListData.Text = "No content available.";
                    }
                    else
                    {
                        btnRemoveFromList.Visibility = Visibility.Visible;
                        txtBlockWatchListData.Text = (lbWatchList.SelectedIndex >= 0) ? GetQuoteData(lbWatchList.SelectedIndex) : "Select a quote to view.";
                        if (lbWatchList.SelectedIndex != lastIndex)
                            lbWatchList.SelectedIndex = lastIndex;
                    }
                    break;
                default:
                    if (!ApplicationSettings.IsModified)
                    {
                        if (txtUpdateFrequency.Text != ApplicationSettings.UpdateFrequency.ToString())
                        {
                            txtUpdateFrequency.Text = ApplicationSettings.UpdateFrequency.ToString();
                            sldUpdateFrequency.Value = ApplicationSettings.UpdateFrequency;
                        }

                        if (cbDefaultTab.SelectedIndex != ApplicationSettings.DefaultTab)
                            cbDefaultTab.SelectedIndex = ApplicationSettings.DefaultTab;
                    }

                    if (txtBlockSettingsMessage.Visibility == Visibility.Visible)
                        txtBlockSettingsMessage.Visibility = Visibility.Hidden;
                    break;
            }
        }

        private string GetQuoteData(int p)
        {
            DataBuilder.Clear().Append("Symbol: ").Append(SymbolList[p].Symbol).Append("Price: ").AppendLine(SymbolList[p].LastTrade);
            return DataBuilder.ToString();
        }

        public void GetQuotes(object sender, DoWorkEventArgs e)
        {
            string args = e.Argument.ToString();
            int respCount = 0;
            bool isCancel = false;

            while (respCount < 3)
            {
                if (!BackgroundQuotes.CancellationPending)
                {
                    if (!ApplicationSettings.Debug)
                    {
                        var response = (ResponseQuery)QuoteData.GetResponse(args.Substring(3), QuoteObjectType.AsSerializedQuote);

                        if (!BackgroundQuotes.CancellationPending)
                        {
                            if (response.Status == ResponseCode.OK)
                            {
                                if (args.Substring(0, 2) == "wl")
                                    BackgroundQuotes.ReportProgress(1, response.GetAll());
                                else
                                {
                                    OpenQuote = response.GetFirst();

                                    if (OpenQuote.IsNew && OpenQuote.Symbol != Quote.NOVALUE)
                                        OpenQuote.IsNew = false;
                                }
                            }
                            DataResponseStatus = response.Status;

                            isCancel = (DataResponseStatus == ResponseCode.OK);
                        }
                        else
                            isCancel = true;
                    }
                    else
                    {
                        if (args.Substring(0, 2) == "wl")
                        {
                            Quote[] fakeQuotes = new Quote[SymbolList.Count];
                            for (int x = 0; x < SymbolList.Count; x++)
                            {
                                fakeQuotes[x] = new Quote(SymbolList[x]);
                                fakeQuotes[x].LastTrade = rand.Next(100).ToString();
                            }
                            BackgroundQuotes.ReportProgress(1, fakeQuotes);
                        }
                        else
                        {
                            OpenQuote = new Quote();
                            OpenQuote.IsNew = true;
                            OpenQuote.Symbol = args.Substring(3);
                            OpenQuote.LastTrade = rand.Next(100).ToString();
                        }
                        isCancel = true;
                    }
                }
                else
                    isCancel = true;

                respCount = (isCancel) ? 3 : respCount + 1;
            }
        }


        private void ForceThreadCancel()
        {
            QuoteUpdateTimer.Stop();
            BackgroundQuotes.CancelAsync();
            System.Threading.Thread.Sleep(250);
        }

        #region Tab 1 - Quick Quote

        #region Events

        private void bQuickSearch_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtQuoteBox.Text.Trim()))
            {
                if (!BackgroundQuotes.IsBusy)
                {
                    txtBlockStockName.Text = "Getting quote, please wait...";
                    bQuickSearch.IsEnabled = false;
                    BackgroundQuotes.RunWorkerAsync("cq-" + txtQuoteBox.Text.Trim());
                }
            }
            else
            {
                txtBlockStockName.Text = "Enter a Quote symbol first.";
            }
        }

        private void bAddWatchlist_Click(object sender, RoutedEventArgs e)
        {
            if (!OpenQuote.IsNew)
            {
                if (!SymbolList.HasSymbol(OpenQuote.Symbol))
                {
                    SymbolList.Add(new Quote(OpenQuote));

                    tabController.SelectedIndex = 1;
                }
                else
                {
                    MessageBox.Show("Quote already on watchlist.");
                }
            }
        }

        private void txtQuoteBox_KeyDown(object sender, KeyEventArgs e)
        {
            e.Handled = !(e.Key == Key.Enter) ? !(QuoteWebUtility.IsAlphanumeric(e.Key.ToString())) : false;
        }

        #endregion

        #endregion

        #region Tab 2 - Watch List

        #region Events

        private void ForceWatchUpdate(object sender, EventArgs e)
        {
            ForceThreadCancel();
            UpdateWatchList(sender, e);
            QuoteUpdateTimer.Start();
        }

        private void UpdateWatchList(object sender, EventArgs e)
        {
            if (SymbolList.Count > 0 && !BackgroundQuotes.IsBusy)
                BackgroundQuotes.RunWorkerAsync("wl-" + SymbolList.GetSymbolQuery());
        }

        private void WatchListChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add
                || e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Remove)
            {

                if (SymbolList.Count > 0)
                {
                    if (!QuoteUpdateTimer.IsEnabled)
                        QuoteUpdateTimer.Start();

                    menuSaveWatchList.IsEnabled = true;
                }
                else
                {
                    QuoteUpdateTimer.Stop();
                    menuSaveWatchList.IsEnabled = false;
                }
                UpdateView(tabController.SelectedIndex);
            }
        }

        private void btnRemoveFromList_Click(object sender, RoutedEventArgs e)
        {
            if (lbWatchList.SelectedIndex >= 0)
                SymbolList.RemoveAt(lbWatchList.SelectedIndex);
        }

        private void lbWatchList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbWatchList.SelectedIndex < 0)
                e.Handled = true;
            else
            {
                lastIndex = lbWatchList.SelectedIndex;
                UpdateView(tabController.SelectedIndex);
            }
        }

        #endregion

        #endregion

        #region Tab 3 - Settings

        private void UpdateActiveSettings()
        {
            // Timer update
            QuoteUpdateTimer.Interval = ApplicationSettings.GetUpdateTimeSpan();
        }

        #region Events

        private void bUpdateSettings_Click(object sender, RoutedEventArgs e)
        {
            ApplicationSettings.UpdateFrequency = int.Parse(txtUpdateFrequency.Text);
            ApplicationSettings.DefaultTab = cbDefaultTab.SelectedIndex;
            ApplicationSettings.Save();

            UpdateActiveSettings();
            txtBlockSettingsMessage.Visibility = Visibility.Visible;
        }

        private void sldUpdateFrequency_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            txtUpdateFrequency.Text = e.NewValue.ToString();
            if(ApplicationSettings != null)
                ApplicationSettings.IsModified = true;
        }

        private void cbDefaultTab_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ApplicationSettings.IsModified = true;
        }

        #endregion

        #endregion

        #region Window Events and Controls

        private void tabController_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl)
            {
                if (e.AddedItems.Count > 0)
                    UpdateView(tabController.SelectedIndex);
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                var openDlg = new OpenFileDialog();
                openDlg.FileName = "watchlist.dat";
                openDlg.DefaultExt = ".dat";
                openDlg.Filter = "Data Files|*.dat";

                Nullable<bool> result = openDlg.ShowDialog();
                if (result != null && result == true)
                {
                    var list = SettingsUtility.LoadWatchList(openDlg.FileName);
                    if (list.Count > 0)
                    {
                        SymbolList.CollectionChanged -= WatchListChanged;
                        SymbolList.Clear();
                        for (int j = 0; j < list.Count; j++)
                            SymbolList.Add(list[j]);
                        SymbolList.CollectionChanged += WatchListChanged;

                        lbWatchList.ItemsSource = SymbolList;
                        tabController.SelectedIndex = 1;
                        ForceWatchUpdate(sender, EventArgs.Empty);
                    }
                }
            }
            catch (Exception ex) { ErrorUtility.Log(ex); }
        }

        private void menuSaveWatchList_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var svDlg = new SaveFileDialog();
                svDlg.FileName = "watchlist.dat";
                svDlg.DefaultExt = ".dat";
                svDlg.Filter = "Data Files|*.dat";

                Nullable<bool> result = svDlg.ShowDialog();
                if (result != null && result == true)
                {
                    if (DirectoryUtility.TryMakePathSafe(svDlg.FileName))
                        SymbolList.Save(svDlg.FileName);
                    else
                        throw new System.IO.FileLoadException();
                }
            }
            catch (Exception ex) { ErrorUtility.Log(ex); }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                SymbolList.Save(SettingsUtility.ApplicationSavePath + "\\" + SettingsUtility.TempWatchList);
            }
            catch (Exception ex)
            {
                ErrorUtility.Log(ex);
            }
        }

        private void SaveCommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (menuSaveWatchList.IsEnabled);
        }

        #endregion

        private void bAlerts_Click(object sender, RoutedEventArgs e)
        {
            if (SymbolList.Count > 0)
            {
                var win = new AlertView(ref SymbolList);
                win.ShowDialog();
            }
        }
    }
}