﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Shapes;

using TickerTape.Models;
using TickerTape.Utilities.Extensions;

namespace TickerTape
{
    /// <summary>
    /// Add and update alerts for select stock quotes.
    /// </summary>
    public partial class AlertView : Window
    {
        private const string ActionButtonUpdate = "Update";

        private QuoteSetViewModel _symbolReference;
        private List<Quote> _tempAlerts;

        public int AlertSelection
        {
            get
            {
                return cbAlertBox.SelectedIndex;
            }
            set
            {
                cbAlertBox.SelectedIndex = value;
                cbAlertBox_SelectionChanged(this, null);
            }
        }

        public AlertView(ref QuoteSetViewModel symbols)
        {
            InitializeComponent();

            _symbolReference = symbols;

            this.Loaded += (s,e) => {
                foreach (Alert.AlertType type in Enum.GetValues(typeof(Alert.AlertType)))
                {
                    if(type != Alert.AlertType.None)
                        cbTypeBox.Items.Add(type.GetDescription());
                }
                cbTypeBox.IsEnabled = false;

                lbSymbolList.ItemsSource = _symbolReference;

                // Copy all quotes and alerts for temp editing store
                _tempAlerts = new List<Quote>();
                for (int x = 0; x < _symbolReference.Count; x++)
                    _tempAlerts.Add(new Quote(_symbolReference[x]));

                ClearControls();
                lbSymbolList.SelectedIndex = 0;
            };
        }

        protected override void OnClosed(EventArgs e)
        {
            lbSymbolList.ItemsSource = null;
            _symbolReference = null;

            base.OnClosed(e);
        }

        private void lbSymbolList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbAlertBox.ItemsSource = null;
            cbAlertBox.ItemsSource = _tempAlerts[lbSymbolList.SelectedIndex].Alerts;

            if (cbAlertBox.Items.Count > 0)
            {
                // pick earliest modified alert or 0
                AlertSelection = 0;
            }
            else
                AlertSelection = -1;
        }

        private void cbAlertBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_tempAlerts[lbSymbolList.SelectedIndex].Alerts.Count > 0)
            {
                cbTypeBox.IsEnabled = true;
                if (_tempAlerts[lbSymbolList.SelectedIndex].Alerts[AlertSelection].Type != Alert.AlertType.None)
                {
                    for (int j = 0; j < cbTypeBox.Items.Count; j++)
                    {
                        if (cbTypeBox.Items[j].ToString() == _tempAlerts[lbSymbolList.SelectedIndex].Alerts[AlertSelection].Type.GetDescription())
                        {
                            cbTypeBox.SelectedIndex = j;
                            break;
                        }
                    }
                }
                else
                    cbTypeBox.SelectedIndex = 0;
            }
            else
                UpdateView(-1);
        }

        private void cbTypeBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UpdateView(cbTypeBox.SelectedIndex);
        }

        private void UpdateView(int alertIndex)
        {
            if (alertIndex >= 0)
            {
                tbxLabel.IsEnabled = true;
                tbxLabel.DataContext = _tempAlerts[lbSymbolList.SelectedIndex].Alerts[AlertSelection];

                tbTarget.Visibility = System.Windows.Visibility.Visible;
                controlContainer.Visibility = System.Windows.Visibility.Visible;
                compPanel.Visibility = System.Windows.Visibility.Collapsed;
                rangePanel.Visibility = System.Windows.Visibility.Collapsed;
                stopPanel.Visibility = System.Windows.Visibility.Collapsed;

                switch (alertIndex)
                {
                    case (int)Alert.AlertType.GreaterThan-1:
                    case (int)Alert.AlertType.LessThan-1: tbTarget.Text = "Target:";
                        compPanel.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case (int)Alert.AlertType.Range-1: tbTarget.Text = "Range:";
                        rangePanel.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case (int)Alert.AlertType.TrailingStop-1: tbTarget.Text = "Percent:";
                        stopPanel.Visibility = System.Windows.Visibility.Visible;
                        break;
                    default: break;
                }
            }
            else
            {
                tbxLabel.DataContext = null;
                tbxLabel.Text = string.Empty;

                tbTarget.Visibility = System.Windows.Visibility.Hidden;
                controlContainer.Visibility = System.Windows.Visibility.Hidden;

                cbTypeBox.IsEnabled = false;
                tbxLabel.IsEnabled = false;
            }
        }

        private void ClearControls()
        {
            tbxComparison.Text = "0.00";
            tbxRangeA.Text = "0.00";
            tbxRangeB.Text = "1.00";
            tbxTrailingStop.Text = "10.0";
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            _tempAlerts[lbSymbolList.SelectedIndex].Alerts.RemoveAt(AlertSelection);
            lbSymbolList_SelectionChanged(this, null);
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e) { this.Close(); }

        private void btnAddAlert_Click(object sender, RoutedEventArgs e)
        {
            _tempAlerts[lbSymbolList.SelectedIndex].Alerts.Add(new Alert(Alert.AlertType.Range, null, "New Alert", new float[]{0f,0f,0f}));
            _tempAlerts[lbSymbolList.SelectedIndex].Alerts[_tempAlerts[lbSymbolList.SelectedIndex].Alerts.Count-1].IsModified = true;

            AlertSelection = _tempAlerts[lbSymbolList.SelectedIndex].Alerts.Count - 1;
        }

        private void btnApply_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnAccept_Click(object sender, RoutedEventArgs e)
        {

        }

        private void tbxLabel_TextChanged(object sender, TextChangedEventArgs e)
        {
            //if(_tempAlerts[lbSymbolList.SelectedIndex].Alerts.Count > 0)
                //_tempAlerts[lbSymbolList.SelectedIndex].Alerts[AlertSelection].IsModified = true;
        }
    }
}
