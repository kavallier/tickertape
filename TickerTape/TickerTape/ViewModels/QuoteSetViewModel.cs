﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace TickerTape.Models
{
    [DataContract]
    public class QuoteSetViewModel : ObservableCollection<Quote>
    {
        private static volatile QuoteSetViewModel single;
        private static object m_lock = new object();

        [IgnoreDataMember]
        public static QuoteSetViewModel Empty
        {
            get
            {
                if (single == null)
                {
                    lock (m_lock)
                    {
                        if (single == null)
                            single = new QuoteSetViewModel();
                    }
                }
                return single;
            }
        }

        public void Update(int index, Quote quote)
        {
            this[index].Update(quote);
            this.OnCollectionChanged(new System.Collections.Specialized.NotifyCollectionChangedEventArgs(System.Collections.Specialized.NotifyCollectionChangedAction.Reset));
        }

        public bool HasSymbol(string symbol)
        {
            return this.Any(quote => quote.Symbol.Equals(symbol));
        }

        public QuoteSymbol[] GetSymbols()
        {
            if (this.Count > 0)
            {
                QuoteSymbol[] symbols = new QuoteSymbol[this.Count];
                for (int i = 0; i < this.Count; i++)
                    symbols[i] = new QuoteSymbol(this[i].Symbol, this[i].Alerts.ToArray());
                return symbols;
            }
            return new QuoteSymbol[]{};
        }

        public string GetSymbolQuery()
        {
            if (this.Count > 0)
            {
                StringBuilder symBld = new StringBuilder();
                foreach (var quote in this)
                {
                    symBld.Append(quote.Symbol + ",");
                }
                symBld.Remove(symBld.Length - 1, 1);
                return symBld.ToString();
            }
            return string.Empty;
        }

        public void Save(string path)
        {
            TickerTape.Utilities.JSONUtility.SaveJSONObject<QuoteSymbol[]>(path, this.GetSymbols());
        }
    }
}
